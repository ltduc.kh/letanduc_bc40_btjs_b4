
const removeWhiteSpace = e=>e.replace(/\s/g, "")
, hasValue = e=>"" !== removeWhiteSpace(e)
, isNum = e=>!isNaN(Number(removeWhiteSpace(e)))
, isInt = e=>(e = Number(removeWhiteSpace(e)),
!!Number.isInteger(e))
, isPositive = e=>(e = Number(removeWhiteSpace(e))) >= 0

let sapXepTang = (...e)=>(e.sort((e,t)=>e - t),
e);
document.getElementById("sortNumber").onclick = function() {
    let e = document.getElementById("inputNum1").value 
      , t = document.getElementById("inputNum2").value 
      , n = document.getElementById("inputNum3").value
      , a = document.getElementById("txtSortNumber");
      if (hasValue(e) && isNum(e) && isInt(e) && hasValue(t) && isNum(t) && isInt(t) && hasValue(n) && isNum(n) && isInt(n)) {
        let i = sapXepTang(Number(removeWhiteSpace(e)), Number(removeWhiteSpace(t)), Number(removeWhiteSpace(n)));
        a.innerHTML = i.join(",")
    } else
        a.innerHTML = "",
        alert("Dữ liệu không hợp lệ")
}
document.getElementById("btnHello").onclick = function() {
    var e = document.getElementById("selUser").value
      , t = document.getElementById("txtHello");
    t.innerHTML = "B" == e ? "Chào Bố ạ!!!!!!!" :
     "M" == e ? "Chào Mẹ ạ!!!!!!!" : 
     "A" == e ? "Chào Anh Trai!!!!!!!" : 
     "E" == e ? "Chào Em gái!!!!!!!" : 
     "Ai Vậy nak!!!!!!!"
}
document.getElementById("btnCount").onclick = function() {
    var e = document.getElementById("inputCount1").value
      , t = document.getElementById("inputCount2").value
      , n = document.getElementById("inputCount3").value
      , a = 0;
    hasValue(e) && isNum(e) && isInt(e) && hasValue(t) && isNum(t) && isInt(t) && hasValue(n) && isNum(n) && isInt(n) ? (Number(removeWhiteSpace(e)) % 2 == 0 && a++,
    Number(removeWhiteSpace(t)) % 2 == 0 && a++,
    Number(removeWhiteSpace(n)) % 2 == 0 && a++,
    document.getElementById("txtCount").innerHTML = "Có " + a + " số chẵn," + (3 - a) + " số lẻ") : (document.getElementById("txtCount").innerHTML = "",
    alert("Dữ liệu không hợp lệ"))
}

let checkCanhTamGiac = (e,t,n)=>e + t > n && e + n > t && t + n > e;
document.getElementById("btnEdge").onclick = function() {
    var e = document.getElementById("inputEdge1").value
      , t = document.getElementById("inputEdge2").value
      , n = document.getElementById("inputEdge3").value
      , a = document.getElementById("txtEdge");
    if (hasValue(e) && isNum(e) && isPositive(e) && hasValue(t) && isNum(t) && isPositive(t) && hasValue(n) && isNum(n) && isPositive(n) && checkCanhTamGiac(Number(e), Number(t), Number(n))) {
        var i = Math.sqrt(Math.pow(t, 2) + Math.pow(n, 2))
          , u = Math.sqrt(Math.pow(e, 2) + Math.pow(n, 2))
          , m = Math.sqrt(Math.pow(e, 2) + Math.pow(t, 2));
        a.innerHTML = e == t && t == n ? "Hình tam giác đều" : e == t || t == n || e == n ? "Hình tam giác cân" : e == i || t == u || n == m ? "Hình tam giác vuông" : "Một loại tam giác khác"
    } else
        a.innerHTML = "",
        alert("Dữ liệu không hợp lệ")
}